import cv2
import numpy as np

input_camera = "y"
if input_camera == "y":
    from intel_realsense_camera import *

# Load the RealSense camera
rs = IntelRealSenseCamera()

while True:
    # Get the Intel RealSense Camera Frame into real time
    ret, color_rgb, depth_color_image, infrared_3d = rs.get_frame_stream()
    cv2.namedWindow("Color, Depth, and Infrared Stream", cv2.WINDOW_KEEPRATIO)

    # Render image in opencv window
    images_depth_infrared = np.hstack((color_rgb, depth_color_image, infrared_3d))
    cv2.imshow("Color, Depth, and Infrared Stream", images_depth_infrared)

    key = cv2.waitKey(1)
    if key & 0xFF == ord("q"):
        break

rs.release()
cv2.destroyAllWindows()