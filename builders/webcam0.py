import cv2
def rescale_frame(frame, scale):    # works for image, video, live video
    width = int(frame.shape[1] * scale)
    height = int(frame.shape[0] * scale)
    dimensions = (width, height)
    return cv2.resize(frame, dimensions, interpolation=cv2.INTER_AREA)


def webcam(cam_id):    
    
    cam = cv2.VideoCapture(cam_id)
    f = open("./builders/testlist.txt","r+")
    f.truncate(0)
    f.close()
    cv2.namedWindow("test")

    img_counter = 0

    while True:
        ret, frame = cam.read()

        if not ret:
            print("failed to grab frame")
            break
        cv2.imshow("test", frame)

        k = cv2.waitKey(1)
        if k%256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break
        elif k%256 == 32:
            # SPACE pressed
            img_name = "opencv_frame_{}.png".format(img_counter)
            #image_path ="web_image/" + img_name
            root_path = "./"
            image_path ="test_predict/" + img_name
            path = root_path + "test_predict/" + img_name
            cv2.imwrite(path, frame)
            print("{} written!".format(img_name))
            img_counter += 1
            file = open("./builders/testlist.txt","a+")
            print(image_path)
            file.write(image_path)
            file.write("\n")
            file. close()

    cam.release()

    cv2.destroyAllWindows()
